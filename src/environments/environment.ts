// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig: {
    apiKey: "AIzaSyDdK-7jVKjEhRokXn32mxnhw80b2CHCyo8",
    authDomain: "test1-e49d1.firebaseapp.com",
    databaseURL: "https://test1-e49d1.firebaseio.com",
    projectId: "test1-e49d1",
    storageBucket: "test1-e49d1.appspot.com",
    messagingSenderId: "987957718043",
    appId: "1:987957718043:web:8fe46d932dcc1b3dd25cbe"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
