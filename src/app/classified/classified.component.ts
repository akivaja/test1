import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ImageService } from '../image.service';


@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;
  
  constructor(public classifyservice:ClassifyService,
              public imageservice:ImageService) { }


              ngOnInit() {
                this.classifyservice.classify().subscribe(
                  res => {
                    this.category = this.classifyservice.categories[res];
                    this.categoryImage = this.imageservice.images[res];
                  }
                )
              }
            

}
